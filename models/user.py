import uuid
from odoo import models, fields, api
from odoo.exceptions import ValidationError
import jwt


class auth(models.Model):
    _name = 'user.auth'
    _description = 'user auth'
    _id =  uuid.uuid1()
    name = fields.Char(required=True)
    email = fields.Char(required=True)
    password = fields.Char(required=True)
    confirmePassword = fields.Char(required=True)
    @api.depends('password' , 'confirmePassword')
    def _check_Password(self):
        for record in self:
            if (not(record.password == record.confirmePassword)):
                raise ValidationError("Incorrect Password confirme !")
    @api.depends('Email')

    def _check_Email(self):
        for record in self:
            if ((len(record.email) < 4) or (not(record.email.find('@')))):
                raise ValidationError("Incorrect Email Format!")