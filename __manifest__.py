{
    'name': 'Rest Auth',
    'version': '1.0.0',
    'category': 'Authentication',
    'summary': 'Rest Authentication',
    'data': [
        'security/ir.model.access.csv',
        'views/users_views.xml'
      ],

    'auto_install': True,
   'license': 'LGPL-3'

}