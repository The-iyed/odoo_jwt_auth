from logging import Logger
import re
import jwt
from odoo import http
from odoo.http import Controller, route, request
from odoo.addons.base.models.ir_http import ir_http
from odoo.exceptions import AccessDenied




class AuthenticationController(http.Controller):

   @classmethod
   def _authenticate(cls, endpoint, request):
      auth = 'none' if http.is_cors_preflight(request, endpoint) else endpoint.routing['auth']

      try:
         if 'Authorization' in request.headers:
            auth_header = request.headers.get('Authorization')
            token = auth_header.split(' ')[1]
            decoded_token = jwt.decode(token, '<secret_key>', algorithms=['HS256'])
            # if the token is expired, jwt.decode will raise a jwt.ExpiredSignatureError exception
         else:
            raise AccessDenied()
        
         getattr(cls, f'_auth_method_{auth}')()
      except (AccessDenied, jwt.ExpiredSignatureError):
         raise
      except Exception:
         Logger.info("Exception during request Authentication.", exc_info=True)
         raise AccessDenied()



   @http.route('/api/login',type='http',method=["POST"])
   def authentication(self,**kwargs):
      email = kwargs.get('email')
      password = kwargs.get('password')

      AuthenticationController.protectRoute(http.request.endpoint, http.request)
      #for route protection

      if not re.match("re[^@]+@[^@]+\.[^@]+",email):
         return http.Response(status=422,json={'error':'Bad email Format'})
      
      user = request.env['res.users'].sudo().search([('email','=',email)])
      if not user :
         return http.Response(status=404,json={'error':'User not Found'})

      if not user.check_password(password):
         return http.Response(status=401,json={'error':'Incorrect password'})

      token = jwt.encode({'email':email,'password':password},'secret',algorithm='HS256')
      return http.Response(status=200,json={'token':token.decode('utf-8')})
   
   @http.route('/<path:path>') 
   def handle_404(self): 
      return "Sorry !! Page not found Status : 404 🥷🏻"